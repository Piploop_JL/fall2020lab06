//Jimmy Le, 1936415
package lab6;
import java.util.Random;

public class RpsGame {
	private int wins;
	private int ties;
	private int losses;
	private Random randgen;
	
	
	public RpsGame() {
		wins = 0;
		ties = 0;
		losses = 0;
		randgen = new Random();
	}
	
	public int getWins() {
		return wins;
	}
	public int getTies() {
		return ties;
	}
	public int getLosses() {
		return losses;
	}
	
	public String playRound(String choice) {
		int player = 0;
		int bot = randgen.nextInt(3);
		String botchoice = "";
		String result = "";
		
		//converting the random number into a choice
		switch(bot) {	
		case 0:
			botchoice = "rock";
			break;
		case 1:
			botchoice = "paper";
			break;
		case 2:
			botchoice = "scissors";
			break;
		}
		
		//converting the player's choice into a number
		switch(choice) {
		case "rock" :
			player = 0;
			break;
		case "paper" :
			player = 1;
			break;
		case "scissors" :
			player = 2;
			break;
		}
		
		//using the numbers to easily compare
		//wins
		if(player == 0 && bot == 2 || player == 1 && bot == 0 || player == 2 && bot == 1) {
			wins++;
			result = " the player won!";
		}
		//losses
		else if(player == 0 && bot == 1 || player == 1 && bot == 2 || player == 2 && bot == 0) {
			losses++;
			result = " the computer won!";
		}
		//ties
		else {
			ties++;
			result = " it's a tie!";
		}
		
		return ("Computer plays " + botchoice + " and" + result);
		
	}
	/*
	public static void main(String[] args) {
		RpsGame test = new RpsGame();
		String choice = "paper";
		String result = test.playRound(choice);
		String result2 = test.playRound(choice);
		String result3 = test.playRound(choice);
		System.out.println(result);
		System.out.println(result2);
		System.out.println(result3);
		System.out.println();
		System.out.println(test.getWins());
		System.out.println(test.getTies());
		System.out.println(test.getLosses());
	}
	*/
	

}
