//Jimmy Le, 1936415
package lab6;
import javafx.application.*;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;

public class RpsApplication extends Application{
	private RpsGame game = new RpsGame();
	
	public void start(Stage stage) throws Exception {
		//boxes
		Group root = new Group();
		VBox vbox = new VBox();
		HBox buttons = new HBox();
		HBox textFields = new HBox();
		
		//buttons
		Button rock = new Button("Rock");
		Button paper = new Button("Paper");
		Button scissors = new Button("Scissors");
		
	
		
		//texts
		TextField welcome = new TextField("Welcome!");
		TextField wins = new TextField("Wins: " + game.getWins());
		TextField losses = new TextField("Losses: " + game.getLosses());
		TextField ties = new TextField("Ties: " + game.getTies());
		//text sizes
		welcome.setPrefWidth(350);
		wins.setPrefWidth(200);
		losses.setPrefWidth(200);
		ties.setPrefWidth(200);
		
		//events
		RpsChoice rockAction = new RpsChoice(welcome,wins,losses,ties,"rock",game);
		rock.setOnAction(rockAction);
		RpsChoice paperAction = new RpsChoice(welcome,wins,losses,ties,"paper",game);
		paper.setOnAction(paperAction);
		RpsChoice scissorsAction = new RpsChoice(welcome,wins,losses,ties,"scissors",game);
		scissors.setOnAction(scissorsAction);
		
		//scene
		Scene scene = new Scene(root,650,300);
		scene.setFill(Color.CORNFLOWERBLUE);
		
		//inserting into boxes
		buttons.getChildren().addAll(rock,paper,scissors);
		textFields.getChildren().addAll(welcome,wins,losses,ties);
		vbox.getChildren().addAll(buttons,textFields);
		root.getChildren().add(vbox);
		
		
		//stages
		stage.setTitle("Rock Paper Scissors");
		stage.setScene(scene);
		stage.show();
		
	}
	
	public static void main(String[] args) {
		Application.launch(args);
	}

}
