//Jimmy Le, 1936415
package lab6;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.*;

public class RpsChoice implements EventHandler<ActionEvent>{
	private TextField message;
	private TextField wins;
	private TextField losses;
	private TextField ties;
	private String choice;
	private RpsGame game;
	
	public RpsChoice(TextField message, TextField wins, TextField losses, TextField ties, String choice, RpsGame game) {
		this.message = message;
		this.wins = wins;
		this.losses = losses;
		this.ties = ties;
		this.choice = choice;
		this.game = game;
	}

	@Override
	public void handle(ActionEvent e) {
		String result = game.playRound(choice);
		
		message.setText(result);
		wins.setText("Wins: " + game.getWins());
		losses.setText("Losses: " + game.getLosses());
		ties.setText("Ties: " + game.getTies());
		
	}
	

}
